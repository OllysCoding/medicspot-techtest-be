require('dotenv').config()

const express = require('express')
const app = express()

const database = require('./lib/database')

app.get('/locations', async(req, res, next) => {
  try {
    const query = database.cleanFuzzyQueryString(req.query.q)
    if (!query || query.length <= 2) 
      return next({ message: 'Search query invalid or too short (must be > 2 characters)', status: 400 })
    
    const results = await database.fuzzyLocationSearch(query)

    res.json(results)
  } catch (err) {
    next(err)
  }
})


app.use((err, req, res, next) => {
  res.status(err.status || 500).json({ message: err.message })
})

app.listen(process.env.PORT || 3000, () => console.log(`Server listening on port: ${process.env.PORT || 3000}`))