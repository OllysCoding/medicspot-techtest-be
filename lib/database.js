const sqlite3 = require('sqlite3')
const db = new sqlite3.Database(process.env.DB_PATH, sqlite3.OPEN_READONLY)

const self = module.exports = {
  /**
   * Perform case-insensitive fuzzy lookup on name
   * @param {String} query 
   * @param {String?} escape (optional )Single escape character
   * @returns {Array
   */
  fuzzyLocationSearch: async (query = '', escape) => new Promise((resolve, reject) => {
    const escapeStatement = escape ? `ESCAPE('${escape}')` : ''
    db.all(
      `SELECT name, latitude, longitude FROM locations WHERE upper(name) LIKE(?) ${escapeStatement}`, 
      `%${query.toUpperCase()}%`, 
      (err, rows) => {
        if (err) reject(err)
        else resolve(rows)
      }
    )
  }),
  /**
   * Cleans '%', '_' and '[ ... ]' from a query string to avoid fetching the entire DB.
   * @param {String} query 
   * @returns {String}
   */
  cleanFuzzyQueryString: (query = '') => 
    query.replace(/(\[.*\])|(%)|(_*)/g, ''),
  /**
   * Escapes each character from a string so that it can't be used for fuzzy matching
   * @param {*} query 
   */
  escapeFuzzyQueryString: (query = '', escapeChar = '!') => {
    let escaped = ''
    for (let char of query) escaped += escapeChar + char
    return escaped
  },
}