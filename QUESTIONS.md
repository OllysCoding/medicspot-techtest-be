# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

The output would be 
```
2
1
```
First, `setTimeout(...)` is executed, where it is passed a function, which will be called after the number of miliseconds specified by the 2nd parameter, in this case `100`.

Then the second console log is called immediately, outputting to the console. 

After 100ms has passed from the `setTimeout` call, it will call the function we passed, outputting `1` to console from our `console.log` call.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

The output of this recursive would count down from 10 to 0, e.g
```
10
9
...
1
0
```

When we first call `foo(d)` with our initial value of `0`, it first performs a check to see if the parameter we passed, `d`, is less than 10. If this is the case it recursively calls itself, passing a value one higher than `d`. In the first call this would be `1`. 

Since each `foo(d)` call must first return before the `console.log` is called, it will first interate all the way to `foo(10)`. At this point, as the condition `d < 10` is not met, it will continue and log out `d`, meaning the first log would be `10`. At this point `foo(10)` returns to its caller: `foo(9)`. `foo(9)`, woul thend log its own parameter before returning.
This process will continue until we reach the initial value we passed to `foo(d)`.

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

As `0` evaluates to falsey when making a weak comparison, if you were to call `foo(0)` it would log out `5` rather `0`. This can be solved with a hard comparison `d === undefined ? d : 5`.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

The output is `3`. This example uses function currying to perform an addition. when calling `foo`, you pass it a single parameter, in this case `1`. It then returns a function which is within the scope of `foo`, giving it access to the parameter you passed. You can then call this returned function, passing a numbe; It will then add the initial value passed to `foo` and the new value you passed, and return the result.

This means the function returned, in this case `bar`, is essentially a function you can call as many times as you want to complete the equation `1 + x`, where `x` is the passed value.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

This function can be called to multiple a number, the first parameter, and the value is passed to a callback function which should accept a parameter as the `result`. This callback function can then use the result of the multiplication. 

As `double` calculates the result within a setTimeout, the callback function will be called aprx. 100ms after you call `double`.

e.g

```js
double(10, (result) => console.log(result))
```
