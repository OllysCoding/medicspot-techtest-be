# Medicspot Technical Test - BE

Backend Node.js & Express.js API for Medicspot Technical Test

## Getting Started

Pre-requisites:
- recent verison of `node.js`
- `sqlite3`

To get up and running do the following

1. Clone the repo: `git clone git@gitlab.com:OllysCoding/medicspot-techtest-be.git`
1. Install packages with `npm install`
1. To get an sqlite database up and running run `./scripts/importer.sh` in the project directory.
1. Set up a `.env` by copying `.env.example`. If you used `importer.sh` set `DB_PATH` to `./data/gb.sl3`. If you want a port other than `3001` you can set it here too.
1. Run in dev mode with `npm run start:dev`, this uses `nodemon` as a file watcher.

### Production

To run in production use `npm run start`

## Rationale

When starting out my first goal was to get the database into SQLite3, a relatively quick process, assuming I ignored the errors about differences in the number of fields for some rows (which I did). I also quickly put together a shell script which can be used to do this automatically. I ended up having to use `sh` instead of `bash` for this script, as bash didn't like my use of `\n` to send multiple commands SQLite. There was likely a solution to this but I decided my time would be better spent on building the actual API, happy the script was there as a nice to have. I definitely also could have added some parameters for an input, output, and table name, which might have made for a more versatile usecase.

At this point I installed the packages I knew I'd need, `express`, `nodemon`, `dotenv` and `sqlite3`, and set to work building the API.

I started out with the super-duper basics, express spins up, dotenv reads the env file. At this point I put in the request handler for the endpoint required, which would allow me to start testing the database implementation as soon as it was ready. For the database implementation, I decided not to go with a `FTS3` table and just use the `LIKE` operator for querying, I quickly determined with would be quick enough for the API, but did take not that I wouldn't be able to sort the results by closet match with this method (at least through the SQL query alone).

In writing my database implementation I quickly noticed a problem. If the user sent in the string `%%%`, the entire databse would be returned. While the `sqlite3` package does sanitize to prevent injection, it did not sanitize `LIKE` query options (`%`, `_` & `[ ... ]`). I was split on two options to prevent this, the first solution I implemented was simply to use regex to remove all these special characters. While confirming that I'd got all the special characters used in the `LIKE` query I also encountered I could use `ESCAPE` to prevent this. As an alternate solution, I wrote a function which would turn the string `test` into `!t!e!s!t` (assuming the escape character picked was `!`). I deliberated over these two solutions for a short while, I decided in the end to use regex, as it seemed simpler to follow, and from my testing I couldn't see a way to get around it. I did leave the second code implementation, so it could be swapped in with a very quick code change. 

I then quickly tested my implentation, and went onto the frontend, returning to do final testing and write up this block of text which I've labelled 'Rationale' but could be bettter described as 'Ramblings of a sleep deprived developer'. 

And finally it was at this point which I realised that you guys had left a html file in the public directory to put the frontend code in, and I created an entire second repository. The self-pitying sigh from this was only second to that one time time I spent 15 minutes debugging some code only to realise I'd never saved one of the files and once I'd saved it, it all immeditately started working.